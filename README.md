# Ngpack

封装一些常用的工具类库以及常用的`angular`组件封装

## 库

`@hedou/core` 基础库，一些常用方法封装
`@hedou/echart` echarts 组件封装，对外只有一个组件，所有chart都通过options实现

## 使用

`npm install @hedou/core`
`npm install @hedou/echart`

## TODO

其他常用组件后续添加
