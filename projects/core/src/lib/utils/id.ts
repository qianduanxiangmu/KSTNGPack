export class IdGenerater {
    static uuid(len: number, radix?: number): string {
        const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
        // tslint:disable-next-line: one-variable-per-declaration
        let uuid = [], i;
        radix = radix !== undefined ? radix : chars.length;

        if (len) {
            // Compact form
            // tslint:disable-next-line: no-bitwise 
            for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
        } else {
            // rfc4122, version 4 form
            let r;
            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    // tslint:disable-next-line: no-bitwise
                    r = 0 | Math.random() * 16;
                    // tslint:disable-next-line: no-bitwise
                    uuid[i] = chars[(i === 19) ? (r & 0x3) | 0x8 : r];
                }
            }
        }
        return uuid.join('');
    }

    static guid() {
        function S4() {
            // tslint:disable-next-line: no-bitwise
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }
        return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4());
    }

}

