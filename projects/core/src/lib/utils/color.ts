// @dynamic
export class ColorUtil {

    static colorRGB() {
        let r = Math.floor(Math.random() * 255);
        let g = Math.floor(Math.random() * 255);
        let b = Math.floor(Math.random() * 255);
        return 'rgba(' + r + ',' + g + ',' + b + ',0.8)';
    }

    static randomColor() {
        return '#' + Math.floor(Math.random() * 0xffffff).toString(16).padEnd(6, '0');
    }

    
    static randomColor4() {
        // tslint:disable-next-line: no-bitwise
        return '#' + (Math.random() * 0xffffff << 0).toString(16);
    }

    static randomColor5() {
        return '#' + function (color) {
            return new Array(7 - color.length).join('0') + color;
            // tslint:disable-next-line: no-bitwise
        }((Math.random() * 0 * 1000000 << 0).toString(16));
    }

    static randomColor6() {
        // tslint:disable-next-line: no-bitwise
        return '#' + ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).substr(-6);
    }

    static randomColor7() {
        let colorAngle = Math.floor(Math.random() * 360);
        return 'hsla(' + colorAngle + ',100%,50%,1)';
    }

}
