import { Injectable } from '@angular/core';

@Injectable()
export class SrciptLoader {

    srcipts: string[] = [];
    constructor() {

    }

    loadStrategy = {
        js: (src) => {
            let el = document.createElement('script');
            el.src = src + '?t=' + new Date().getTime();
            el.async = true;
            return el;
        },
        css: (src) => {
            let el = document.createElement('link');
            el.type = 'text/css';
            el.rel = 'stylesheet';
            el.href = src;
            return el;
        }
    };

    remove(src: string) {
        let scriptIndex = this.srcipts.findIndex(m => m === src);
        if (scriptIndex >= 0) {
            let tags = document.getElementsByTagName('script');
            let scripts = Array.from(tags);
            let index = scripts.findIndex(s => s.src.indexOf(src) >= 0);
            if (index >= 0) {
                scripts[index].parentNode.removeChild(scripts[index]);
            }
            this.srcipts.splice(scriptIndex, 1);
        }

    }

    load(src: string, options?: any) {

        const self = this;
        return new Promise(function (resolve, reject) {

            if (self.srcipts.findIndex(m => src === m) >= 0) {
                resolve();
            } else {
                let ext = src.split('.').pop().toLowerCase();
                if (!(ext in self.loadStrategy)) {
                    throw new Error('Invalid file extension in ' + src);
                }
                let el = self.loadStrategy[ext](src);
                el.onload = () => {
                    self.srcipts.push(src);
                    resolve();
                };
                el.onerror = reject;
                options && Object.keys(options).forEach(function (key) {
                    el.setAttribute(key, options[key]);
                });
                document.head.appendChild(el);
            }
        });
    }

    loadMulti(srcs: string[], options?: any) {
        return Promise.all(srcs.map((src) => {
            return this.load(src, options);
        }));
    }

}
