import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class KEventService {

    private _callbacks: {
        [key: string]: (() => void)[]
    } = {};
    constructor() {

    }

    on(eventName, callback) {
        if (!this._callbacks[eventName]) {
            this._callbacks[eventName] = [];
        }
        this._callbacks[eventName].push(callback);
    }

    off(eventName, callback?) {
        let callbacks = this._callbacks[eventName];
        if (!callbacks) {
            return;
        }

        if (callback !== undefined) {
            let index = -1;
            for (let i = 0; i < callbacks.length; i++) {
                if (callbacks[i] === callback) {
                    index = i;
                    break;
                }
            }

            if (index < 0) {
                return;
            }

            this._callbacks[eventName].splice(index, 1);
        } else {
            this._callbacks[eventName] = undefined;
        }

    }

    trigger(eventName, ...args) {
        let callbacks = this._callbacks[eventName];
        if (!callbacks || !callbacks.length) {
            return;
        }

        callbacks.forEach((c, i) => {
            c.apply(this, args);
        });

    }

}
