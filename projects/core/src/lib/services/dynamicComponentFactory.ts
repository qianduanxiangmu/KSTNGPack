import {
    Injectable, Injector, ApplicationRef, ComponentFactoryResolver, ComponentRef, EventEmitter, Type
} from '@angular/core';
import * as _ from 'lodash';


@Injectable()
export class DynamicDomcponentFactory {

    constructor(
        private injector: Injector,
        private applicationRef: ApplicationRef
    ) {

    }

    /**
     * 
     * @param id 
     * @param componentFactoryResolver 
     * @param component 
     * @param parent 
     * @param attrs 
     * @param events 需要监听的事件列表
     * @param removed 组件被删除后触发
     */
    create(
        id: string,
        componentFactoryResolver: ComponentFactoryResolver,
        component: Type<any>,
        parent: HTMLElement,
        attrs: { [key: string]: any },
        events: {
            name: string,
            cb: (data: any) => void
        }[],
        removed?: () => void) {

        // 构建器
        const factory = componentFactoryResolver.resolveComponentFactory(component);
        // dom选择器
        const domSelector = document.createElement(factory.selector);
        domSelector.setAttribute('id', id);
        // 组件索引
        const componentRef = factory.create(this.injector, [], domSelector);

        // 约定在组件ngOnDestroy生命周期中触发closed事件 
        componentRef.instance.closed && componentRef.instance.closed.subscribe(() => {
            parent.removeChild(domSelector);
            this.applicationRef.detachView(componentRef.hostView);
            // componentRef.destroy();
            removed && removed();
        });

        // 设置属性
        this.setAttr(componentRef, attrs);
        if (events) {
            events.forEach(m => {
                if (!componentRef.instance[m.name]) return;
                (componentRef.instance[m.name] as EventEmitter<any>).subscribe(options => {
                    m.cb({ options, name: component.name });
                });
            });
        }
        // 监听组件 添加脏检查
        this.applicationRef.attachView(componentRef.hostView);
        parent.appendChild(domSelector);
        return componentRef;
    }

    /**
     * 设置属性
     * @param componentRef
     * @param attrs
     */
    setAttr(componentRef: ComponentRef<any>, attrs: { [key: string]: any }) {

        if (attrs === undefined) {
            return;
        }

        for (const key in attrs) {
            if (attrs.hasOwnProperty(key)) {
                const attrVal = attrs[key];
                if (typeof (componentRef.instance[key]) === 'object') {
                    componentRef.instance[key] = _.assign(componentRef.instance[key], attrVal);
                } else {
                    componentRef.instance[key] = attrVal;
                }
            }
        }
    }

}
