import { Injectable, NgModuleFactoryLoader, Injector, ComponentRef } from '@angular/core';
import { DynamicDomcponentFactory } from './dynamicComponentFactory';
import { IdGenerater } from '../utils/id';

@Injectable()
export class LazyModuleLoader {

    private modules: { path: string, instance: any }[] = [];
    private componentRefs: { id: string, comref: ComponentRef<any> }[] = [];
    constructor(
        // tslint:disable-next-line: deprecation
        private loader: NgModuleFactoryLoader,
        private injector: Injector
    ) {

    }



    /**
     * 懒加载模块并创建一个组件
     * @param modulePath path#modelname
     * @param selector component selector
     * @param parent 父节点
     * @param removed 组件被删除后触发
     */
    createComponent(modulePath: string, selector: string, parent: HTMLElement, attrs: any, events: any[] = [], removed?: () => void) {
        const self = this;
        let id = (attrs && attrs.id) ? attrs.id : IdGenerater.uuid(32);
        return self.load(modulePath).then(modRef => {
            if (!modRef) return null;
            // modRef模块应用对象
            // 懒加载的模块约定：
            // 模块类必须定义一个 components 数组
            // components 数组存放模块中对外开放的组件集合
            // 组件描述结构接下：
            // {selector:'选择器',conponent:'组件类对象'}
            // components 数组必须在模块构造时初始化
            if (modRef.components && modRef.components.length) {
                let com = (modRef.components as any[]).find(m => m.selector === selector);
                if (com) {
                    let dcr = modRef.injector.get(DynamicDomcponentFactory) as DynamicDomcponentFactory;
                    let componentRef = dcr.create(id, modRef.componentFactoryResolver, com.component, parent, attrs, events, removed);
                    return componentRef;
                }
            }
            return null;
        }).then(ref => {
            if (ref) {
                this.addRef(id, ref);
            }
            return { id, ref };
        });
    }


    /**
     * 懒加载模块
     * @param path 
     */
    load(path: string) {

        if (!path) return Promise.resolve();
        if (this.modules.findIndex(m => m.path === path) >= 0) {
            return new Promise<any>((reslove, reject) => {
                reslove(this.modules.find(m => m.path === path).instance);
            });
        }

        return this
            .loader
            .load(path)
            .then(moduleFactory => {

                if (this.modules.findIndex(m => m.path === path) < 0) {
                    let moduleRef = moduleFactory.create(this.injector).instance;
                    this.modules.push({ path, instance: moduleRef });
                    return moduleRef;
                } else {
                    return this.modules.find(m => m.path === path).instance;
                }
            })
            .catch(err => {
                console.error('error loading module', err);
            });
    }


    addRef(id: string, comref: ComponentRef<any>) {
        if (this.componentRefs.findIndex(m => m.id === id) < 0) {
            this.componentRefs.push({ id, comref });
        }
    }

    findComponentRef(id: string) {
        return this.componentRefs.find(m => m.id === id);
    }

    /**
     * 注销组件
     * @param id 
     */
    destroy(id: string) {
        let com = this.findComponentRef(id);
        if (com) {
            let index = this.componentRefs.findIndex(m => m.id === id);
            com.comref.destroy();
            this.componentRefs.splice(index, 1);
        }
    }

}
