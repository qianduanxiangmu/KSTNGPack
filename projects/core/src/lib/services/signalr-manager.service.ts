import { Injectable, NgZone } from '@angular/core';
import {
    HubConnectionBuilder,
    HubConnection,
    LogLevel,
    HttpTransportType,
    HubConnectionState
} from '@aspnet/signalr';

import { timer, Subscription, Subject } from 'rxjs';
import * as _ from 'lodash';
import { take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SignalrManager {

    conns: {
        url: string,
        conn: HubConnection,
        components: { id: string, methods: { name: string, callback: (data: any) => void }[] }[]
    }[] = [];

    connecting: boolean = false;
    // 待连接队列
    connectQueue: { url: string, id: string, methods: { name: string, callback: (data: any) => void }[] }[];
    createConnectTimer: Subscription;

    constructor(
        zone: NgZone
    ) {
        this.connectQueue = [];

        zone.runOutsideAngular(() => {
            return this.startQueue();
        });

    }


    /**
     * 启动定时任务
     */
    private startQueue() {
        let maxConnectionCount = 100; // 必须在 maxConnectionCount 秒内完成连接
        this.createConnectTimer = timer(1000, 1000)
            // .pipe(take(maxConnectionCount))
            .subscribe(() => {
                if (this.connecting) return;
                if (!this.connectQueue.length) return;
                let ele = this.connectQueue.splice(0, 1)[0];
                this.dispose(ele.id);
                this.create(ele.id, ele.url, ele.methods);
            });

    }

    /**
     * 创建连接
     * @param url 
     */
    private createConnection(url: string): HubConnection {

        if (this.conns.findIndex(m => m.url === url) >= 0) {
            return this.conns.find(m => m.url === url).conn;

        } else {

            let conn = new HubConnectionBuilder()
                .withUrl(url, {
                    skipNegotiation: true,
                    transport: HttpTransportType.WebSockets
                })
                .configureLogging(LogLevel.Debug)
                .build();

            this.conns.push({
                url,
                conn,
                components: []
            });
            return conn;
        }



    }

    /**
     * 启动连接
     * @param connection 
     */
    private start(connection: HubConnection): Promise<HubConnection> {

        this.connecting = true;
        if (connection.state !== HubConnectionState.Disconnected) {
            return new Promise((reslove, reject) => {
                reslove(connection);
                this.connecting = false;
            });
        }

        return new Promise((reslove, reject) => {
            connection.start()
                .then(() => {
                    console.log('connection started');
                    this.connecting = false;
                    connection.onclose((errors) => {
                        console.log('connection closed', errors);
                        console.log('state:', connection.state);
                        if (!!errors) {
                            // 意外断开 尝试重连
                            this.reconnect(connection);
                        }
                    });
                    reslove(connection);
                })
                .catch(err => {
                    console.log(err);
                    this.connecting = false;
                    reject(err);
                });
        });
    }

    /**
     * 重新连接
     * @param connection 
     */
    private reconnect(connection: HubConnection) {
        let self = this;

        let connectTimer = timer(0, 1000)
            .subscribe(() => {
                if (this.connecting) return;
                console.log('reconnect...');
                console.log(`当前状态：【${connection.state}】`);

                if (connection.state === HubConnectionState.Connected) {
                    connectTimer.unsubscribe();
                    return;
                }
                self.start(connection).then(() => {
                    console.log('reconnect success!');
                    connectTimer.unsubscribe();
                });
            });

    }

    private configComponent(id: string, url: string, eventNames: { name: string, callback: (data: any) => void }[]) {
        // 查找组件
        let connConfig = this.conns.find(m => m.url === url);
        let comIndex = connConfig.components.findIndex(m => m.id === id);

        if (comIndex >= 0) {
            connConfig.components[comIndex].methods = _.uniq(connConfig.components[comIndex].methods.concat([...eventNames]));

        } else {
            connConfig.components.push({ id, methods: [...eventNames] });
        }

        eventNames.forEach(item => {
            // 约定所有的方法只有一个参数 并就是当前数据直接返回
            // 通过事件来传输数据
            console.log(`事件名称:【${item.name}】【${url}】`);
            console.log(connConfig.conn);

            connConfig.conn.on(item.name, (...args) => this.onRecevied(args, id, item.callback));
        });

    }

    private onRecevied(data, id: string, callback: (data: any) => void) {
        console.log(`【${id}】recevied message,msg Name:【${name}】`, data);
        // 处理
        callback(data);
    }

    /**
     * 创建一个signalr连接
     * @param id 组件id
     * @param url 连接地址
     * @param eventNames 接收事件名称集合
     */
    create(id: string, url: string, eventNames: { name: string, callback: (data: any) => void }[]) {

        if (this.connecting) {
            return new Promise((reslove, reject) => {
                reslove();
            });
        }


        const connection = this.createConnection(url);

        this.configComponent(id, url, eventNames);
        return this.start(connection);
    }

    dispose(id: string) {
        // let configs = this.conns.filter(m => m.components.findIndex(c => c.id === id) >= 0);

        this.conns.forEach((c, j) => {
            if (!c.conn) return true;
            c.components.forEach((com) => {
                if (com.id === id) {
                    com.methods.forEach(m => {
                        c.conn.off(m.name);
                    });
                }
            });
            let componentIndex = c.components.findIndex(m => m.id === id);
            c.components.splice(componentIndex, 1);

        });

    }

}

