import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimerService } from './http/timer.service';
import { LazyModuleLoader } from './services/lazy-module.loader';
import { DynamicDomcponentFactory } from './services/dynamicComponentFactory';
import { SrciptLoader } from './services/script-loader';

@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule
  ],
  exports: [
    
  ],
  providers: [
    LazyModuleLoader,
    DynamicDomcponentFactory,
    TimerService,
    SrciptLoader
  ]
})
export class CoreModule { }
