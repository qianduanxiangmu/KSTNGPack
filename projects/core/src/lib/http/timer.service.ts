import { Injectable } from '@angular/core';
import { timer, Subscription } from 'rxjs';
/**
 * 定时服务
 */
@Injectable()
export class TimerService {

    instance: {
        [key: string]: {
            item?: Subscription,    // 可订阅对象
            opt: any,       // 参数
            cb: (opt: any) => void      // 回调
        };
    };

    constructor() {
        this.instance = {};
    }

    /**
     * 启动一个循环任务
     * @param interval 时间间隔（秒）
     */
    startInterval<TOpt>(name: string, opt: TOpt, interval: number, cb: (opt: TOpt) => void) {
        if (this.instance[name]) {
            this.remove(name);
        }
        this.instance[name] = {
            opt,
            cb
        };
        this.instance[name].item = timer(0, interval * 1000).subscribe(() => {
            this.instance[name].cb(this.instance[name].opt);
        });
    }

    /**
     * 替换执行的opt
     * @param name 名称
     * @param opt opt
     * 
     */
    replace(name: string, opt: any) {
        if (!this.instance[name]) return;
        this.instance[name].opt = opt;
        this.instance[name].cb(this.instance[name].opt);
    }


    remove(name: string) {
        if (this.instance[name]) {
            this.instance[name].item.unsubscribe();
            delete this.instance[name];
        }
    }

    clear() {
        for (const key in this.instance) {
            if (this.instance.hasOwnProperty(key)) {
                this.remove(key);
            }
        }

    }
}
