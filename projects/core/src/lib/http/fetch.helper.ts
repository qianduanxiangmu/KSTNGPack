import { from } from 'rxjs';

/**
 * fetch 方法封装
 */
export class FetchHelper {

    static fetchJson(url: string, method: 'GET' | 'POST' = 'GET', data: any = {}, headers: { [key: string]: string } = {}) {
        return method === 'GET' ? FetchHelper.getJson(url) : FetchHelper.postJSON(url, data);
    }

    static getJson(url: string, headers: { [key: string]: string } = {}) {

        const options: any = {
            method: 'GET',
            // 缓存
            cache: 'no-cache',
            headers: new Headers({
                'Content-Type': 'application/json',
                ...headers
            }),
        };
        return from(fetch(url, options).then(response => {
            return response.json();
        }).catch((reson) => {
            console.error(reson);
            return new Promise((r, i) => r(null));
        }));
    }

    /**
     * post获取json
     * @param url 地址
     * @param data 数据
     */
    static postJSON(url: string, data: any = {}, headers: { [key: string]: string } = {}) {

        const content_ = JSON.stringify(data);
        const options_: any = {
            method: 'POST',
            cache: 'no-cache',
            headers: new Headers({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                mode: 'cors',
                body: content_,
                ...headers
            }),
        };

        return from(fetch(url, options_).then(response => {
            return response.json();
        }).catch((reson) => {
            console.error(reson);
            return new Promise((r, i) => r(null));
        }));
    }

}
