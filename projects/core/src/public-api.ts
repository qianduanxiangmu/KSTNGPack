/*
 * Public API Surface of core
 */


export * from './lib/core.module';
export * from './lib/utils/id';
export * from './lib/utils/color';
export * from './lib/services/dynamicComponentFactory';
export * from './lib/services/kevent.service';
export * from './lib/services/lazy-module.loader';
export * from './lib/services/script-loader';
export * from './lib/services/signalr-manager.service';
export * from './lib/http/timer.service';
export * from './lib/http/fetch.helper';
