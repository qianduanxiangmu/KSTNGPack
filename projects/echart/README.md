# Echart

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.14.

## Code scaffolding

Run `ng generate component component-name --project echart` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project echart`.
> Note: Don't forget to add `--project echart` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build echart` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build echart`, go to the dist folder `cd dist/echart` and run `npm publish`.

## Running unit tests

Run `ng test echart` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
