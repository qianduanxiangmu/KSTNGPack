import { Component, OnInit, Renderer2, ViewChild, ElementRef, OnDestroy, AfterViewInit, Input, OnChanges, SimpleChanges, NgZone } from '@angular/core';
import { bind, clear } from 'size-sensor';
import * as echarts from 'echarts';
import * as _ from 'lodash';

@Component({
	selector: 'k-echart',
	template: `
  <div style="width:100%;height:100%" #container gid class="k-echart ui-echart">
    <div class="echart"></div>
  </div>
  `,
	styles: []
})
export class EchartComponent implements OnInit {

	@ViewChild('container', {
		static: true
	}) container: ElementRef;
	chartDom: HTMLDivElement;
	instance: echarts.ECharts;
	@Input() options: any;
	@Input() height: number = 0;

	parentDomResize: () => void;

	constructor(
		private ngZone: NgZone,
		private renderer: Renderer2,
		private el: ElementRef) {

	}

	ngOnInit() {
		console.log('echart init');
		let parentDom = (this.container.nativeElement as HTMLElement);
		if (this.height > 0) {
			this.renderer.setStyle(parentDom, 'height', this.height + 'px');
		}
	}

	// tslint:disable-next-line: use-life-cycle-interface
	ngAfterViewInit(): void {
		const self = this;

		this.chartDom = (this.container.nativeElement as HTMLElement).querySelector('.echart');
		let parentDom = (self.container.nativeElement as HTMLElement);

		let parentHeight = parentDom.clientHeight;
		// console.log('高度', parentHeight);
		// 获取不到宽度 

		self.renderer.setStyle(self.chartDom, 'height', parentHeight + 'px');
		self.renderer.setStyle(self.chartDom, 'width', parentDom.clientWidth + 'px');

		let theme = '';

		const chartInstance = self.ngZone.runOutsideAngular(() => self.instance = echarts.init(self.chartDom, theme || ''));
		// console.log('ngAfterViewInit<--->', self.instance);
		chartInstance.setOption(self.options);
		self.parentDomResize = bind(parentDom, element => {
			self.renderer.setStyle(self.chartDom, 'height', element.offsetHeight + 'px');
			self.renderer.setStyle(self.chartDom, 'width', parentDom.clientWidth + 'px');
			self.ngZone.runOutsideAngular(() => self.instance.resize({
				height: element.offsetHeight,
				width: parentDom.clientWidth,
				silent: true
			}));
		});
	}


	// tslint:disable-next-line: use-life-cycle-interface
	ngOnDestroy(): void {
		console.log('echart组件销毁');
		this.instance.dispose();
		this.instance = undefined;
		if (this.parentDomResize) this.parentDomResize();
	}

	public setOptins(options: any) {
		// 加载前清除
		// this.instance.clear();
		let self = this;
		if (!this.instance) {
			self.ngZone.runOutsideAngular(() => self.instance = echarts.getInstanceByDom(self.chartDom));
		}

		if (this.instance) {

			let opt = {};
			// 直接判断数组长度 
			_.defaultsDeep(opt, this.options, options);
			self.ngZone.runOutsideAngular(() => this.instance.setOption(opt));
		}

	}

	// tslint:disable-next-line: use-life-cycle-interface
	ngOnChanges(changes: SimpleChanges): void {

	}

}
