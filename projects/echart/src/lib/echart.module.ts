import { NgModule } from '@angular/core';
import { EchartComponent } from './echart.component';


@NgModule({
  declarations: [EchartComponent],
  imports: [
  ],
  exports: [EchartComponent]
})
export class EchartModule { }
